import { GraphQLSchema, Source, visit } from "graphql";
import ast from "graphql/language";

export interface IBaseVisitor {
    // Scalars
    IntValue: (_node: ast.IntValueNode) => any;
    FloatValue: (_node: ast.FloatValueNode) => any;
    StringValue: (_node: ast.StringValueNode) => any;
    BooleanValue: (_node: ast.BooleanValueNode) => any;
    ScalarTypeDefinition: (_node: ast.ScalarTypeDefinitionNode) => any;

    Name: (_node: ast.NameNode) => any;
    Document: (_node: ast.DocumentNode) => any;
    OperationDefinition: (_node: ast.OperationDefinitionNode) => any;
    VariableDefinition: (_node: ast.VariableDefinitionNode) => any;
    Variable: (_node: ast.VariableNode) => any;
    SelectionSet: (_node: ast.SelectionSetNode) => any;
    Field: (_node: ast.FieldNode) => any;
    Argument: (_node: ast.ArgumentNode) => any;
    FragmentSpread: (_node: ast.FragmentSpreadNode) => any;
    InlineFragment: (_node: ast.InlineFragmentNode) => any;
    FragmentDefinition: (_node: ast.FragmentDefinitionNode) => any;
    NullValue: (_node: ast.NullValueNode) => any;
    EnumValue: (_node: ast.EnumValueNode) => any;
    ListValue: (_node: ast.ListValueNode) => any;
    ObjectValue: (_node: ast.ObjectValueNode) => any;
    ObjectField: (_node: ast.ObjectFieldNode) => any;
    NamedType: (_node: ast.NamedTypeNode) => any;
    ListType: (_node: ast.ListTypeNode) => any;
    NonNullType: (_node: ast.NonNullTypeNode) => any;
    SchemaDefinition: (_node: ast.SchemaDefinitionNode) => any;
    OperationTypeDefinition: (_node: ast.OperationTypeDefinitionNode) => any;

    ObjectTypeDefinition: (_node: ast.ObjectTypeDefinitionNode) => any;
    FieldDefinition: (_node: ast.FieldDefinitionNode) => any;
    InputValueDefinition: (_node: ast.InputValueDefinitionNode) => any;
    InterfaceTypeDefinition: (_node: ast.InterfaceTypeDefinitionNode) => any;
    UnionTypeDefinition: (_node: ast.UnionTypeDefinitionNode) => any;
    EnumTypeDefinition: (_name: ast.EnumTypeDefinitionNode) => any;
    EnumValueDefinition: (_node: ast.EnumValueDefinitionNode) => any;
    InputObjectTypeDefinition: (_node: ast.InputObjectTypeDefinitionNode) => any;

    // Directives
    DirectiveDefinition: (_node: ast.DirectiveDefinitionNode) => any;
    Directive: (_node: ast.DirectiveNode) => any;

    // Extensions
    SchemaExtension: (_node: ast.SchemaExtensionNode) => any;
    ScalarTypeExtension: (_node: ast.ScalarTypeExtensionNode) => any;
    ObjectTypeExtension: (_node: ast.ObjectTypeExtensionNode) => any;
    InterfaceTypeExtension: (_node: ast.InterfaceTypeExtensionNode) => any;
    UnionTypeExtension: (_node: ast.UnionTypeExtensionNode) => any;
    EnumTypeExtension: (_node: ast.EnumTypeExtensionNode) => any;
    InputObjectTypeExtension: (_node: ast.InputObjectTypeExtensionNode) => any;
}

export interface ISerializable {
    toString: (config?: ConfigType) => string;
}

namespace Syntax {
    export abstract class Type implements ISerializable {
        abstract toString(config?: ConfigType): string;
    }

    export abstract class Definition implements ISerializable {
        abstract toString(config?: ConfigType): string;
    }

    export class TypeIdentifier implements ISerializable {
        readonly name: string;

        constructor(name: string) {
            this.name = name;
        }

        toString(config?: ConfigType): string {
            return `${config?.typesPrefix}${this.name}${config?.typesSuffix}`;
        }
    }

    export class Literal implements ISerializable {
        readonly value: string;

        constructor(val: string = "") {
            this.value = val;
        }

        toString(_config?: ConfigType) {
            return this.value;
        }
    }

    export class ArrayLiteral extends Literal {
        readonly elems: ReadonlyArray<Syntax.Literal>;

        constructor(elems: Syntax.Literal[]) {
            super();
            this.elems = elems;
        }

        toString(config?: ConfigType): string {
            return `[ ${this.elems.map((e: ISerializable) => e.toString(config)).join(", ")} ]`;
        }
    }

    export class ObjectLiteral extends Literal {

    }

    export class Scalar extends Type {
        readonly tp: string;

        constructor(tp: string) {
            super();
            this.tp = tp;
        }

        toString(_config?: ConfigType): string {
            return this.tp;
        }
    }

    export class NamedType extends Type {
        readonly identifier: Readonly<TypeIdentifier>;

        constructor(name: string) {
            super();
            this.identifier = new TypeIdentifier(name)
        }

        toString(config?: ConfigType): string {
            return this.identifier.toString(config);
        }
    }

    abstract class HigherOrderType extends Type {
        readonly underlyingTp: Readonly<Type>;

        protected constructor(tp: Type) {
            super();
            this.underlyingTp = tp;
        }

        abstract toString(): string;
    }

    export class NonNullable extends HigherOrderType {
        readonly underlyingTp: Readonly<Type>;

        constructor(tp: Type) {
            super(tp);
        }

        toString(config?: ConfigType): string {
            return this.underlyingTp.toString(config);
        }
    }

    export class Maybe extends HigherOrderType {
        constructor(tp: Type) {
            super(tp);
        }

        toString(config?: ConfigType): string {
            return `${this.underlyingTp.toString(config)} | null`;
        }
    }

    export class Array extends HigherOrderType {
        constructor(tp: Type) {
            super(tp);
        }

        toString(config?: ConfigType): string {
            return `Array<${this.underlyingTp.toString(config)}>`
        }
    }

    abstract class PropertyLike extends Definition {
        readonly propName: string;
        readonly propType: Readonly<Type>;
        readonly defaultVal?: Readonly<Literal>;

        protected constructor(name: string, tp: Type, defaultVal?: Literal) {
            super();
            this.propName = name;
            this.propType = tp;
            this.defaultVal = defaultVal;
        }

        toString(config?: ConfigType) {
            return `${this.propName}: ${this.propType.toString(config)}`;
        }
    }

    export class Property extends PropertyLike {
        constructor(name: string, tp: Type, defaultVal?: Literal) {
            super(name, tp, defaultVal);
        }

        toString(config?: ConfigType): string {
            const defaultClause = this.defaultVal && config?.generatePropertyDefaults
                ? ` = ${this.defaultVal.toString(config)}`
                : "";
            return `${super.toString(config)}${defaultClause}`;
        }
    }

    export class PureProperty extends Property {
        constructor(name: string, tp: Type, defaultVal?: Literal) {
            super(name, tp, defaultVal);
        }

        toString(config?: ConfigType): string {
            return super.toString(config);
        }
    }

    export class AbstractProperty extends Property {
        constructor(name: string, tp: Type, defaultVal?: Literal) {
            super(name, tp, defaultVal);
        }

        toString(config?: ConfigType): string {
            return `abstract ${super.toString(config)}`;
        }
    }

    export class Argument extends PropertyLike {
        constructor(name: string, tp: Type, defaultVal?: Literal) {
            super(name, tp, defaultVal);
        }

        toString(config?: ConfigType) {
            const defaultClause = this.defaultVal && config?.generateArgumentDefaults
                ? ` = ${this.defaultVal.toString(config)}`
                : "";
            return `${super.toString(config)}${defaultClause}`;
        }
    }

    export class Function extends Type {
        readonly args: ReadonlyArray<Argument>;
        readonly retTp: Readonly<Type>;

        constructor(args: Argument[], ret: Type) {
            super()
            this.args = args;
            this.retTp = ret;
        }

        toString(config?: ConfigType): string {
            const paramList = this.args.map((arg: ISerializable) => arg.toString(config)).join(", ");
            const retType = this.retTp.toString(config);
            return `(${paramList}) => ${retType}`;
        }
    }

    class Body extends Definition {
        readonly props?: ReadonlyArray<Readonly<Property>>;

        constructor(props?: Property[]) {
            super();
            this.props = props;
        }

        toString(config?: ConfigType): string {
            return [
                "{",
                this.props ? this.props.map((prop: ISerializable) => `\t${prop.toString(config)}`).join("\n") : "",
                "}"
            ].join("\n")
        }
    }

    export class Class extends Definition {
        readonly identifier: Readonly<TypeIdentifier>;
        readonly interfaces?: ReadonlyArray<Readonly<NamedType>>;
        readonly superName?: Readonly<NamedType>;
        readonly body: Readonly<Body>;

        constructor(name: string, superName?: NamedType, interfaces?: NamedType[], props?: Property[]) {
            super();
            this.identifier = new TypeIdentifier(name)
            this.interfaces = interfaces;
            this.superName = superName;
            this.body = new Body(props)
        }

        toString(config?: ConfigType): string {
            const nameClause = `class ${this.identifier.toString(config)} `;
            const extendsClaus = this.superName ? `extends ${this.superName.toString(config)} ` : "";
            const implementsClause = !this.interfaces || this.interfaces?.length == 0 ? ""
                : `implements ${this.interfaces.map((iface: ISerializable) => iface.toString(config)).join(", ")} `;
            const bodyClause = this.body.toString(config);

            return [nameClause, extendsClaus, implementsClause, bodyClause].join("");
        }
    }

    export class AbstractClass extends Class {
        constructor(name: string, superName?: NamedType, interfaces?: NamedType[], props?: Property[]) {
            super(name, superName, interfaces, props);
        }

        toString(config: ConfigType): string {
            return `abstract ${super.toString(config)}`;
        }
    }

    export class Interface extends Definition {
        readonly identifier: Readonly<TypeIdentifier>;
        readonly interfaces?: ReadonlyArray<Readonly<NamedType>>;
        readonly body: Readonly<Body>;

        constructor(name: string, interfaces?: NamedType[], props?: PureProperty[]) {
            super();
            this.identifier = new TypeIdentifier(name);
            this.interfaces = interfaces;
            this.body = new Body(props)
        }

        toString(config?: ConfigType) {
            const nameClause = `interface ${this.identifier.toString(config)} `;
            const extendsClause = !this.interfaces || this.interfaces?.length == 0 ? ""
                : `extends ${this.interfaces.map((iface: ISerializable) => iface.toString(config)).join(", ")} `;
            const bodyClause = this.body.toString(config);

            return [nameClause, extendsClause, bodyClause].join("");
        }
    }

    export class TypeAlias extends Definition {
        readonly identifier: Readonly<TypeIdentifier>;
        readonly body: Readonly<Body>;

        constructor(name: string, props?: PureProperty[]) {
            super();
            this.identifier = new TypeIdentifier(name);
            this.body = new Body(props)
        }

        toString(config?: ConfigType): string {
            return `type ${this.identifier.toString(config)} = ${this.body.toString(config)}`;
        }
    }

    export class Module extends Definition {
        readonly definitions: ReadonlyArray<Readonly<Definition>>;

        constructor(defs: Definition[]) {
            super();
            this.definitions = defs;
        }

        toString(config?: ConfigType): string {
            const {noExport, namespaceName} = config!;

            let code = this.definitions.map((def: Definition) => {
                return `${noExport ? "" : "export "}${def.toString(config)}`;
            }).join("\n\n");

            if (namespaceName) {
                code = `${noExport ? "" : "export "}namespace ${namespaceName} \{\n${code}\}`;
            }

            return code;
        }
    }
}

export class TypeVisitor implements Partial<IBaseVisitor> {
    private readonly definitions_: Array<Syntax.Definition> = [];

    private readonly scalarMap_: ConfigType["scalarMap"];
    private readonly skipTypename_: ConfigType["skipTypename"];

    private readonly typeHandlerMap_: { [index: string]: (node: ast.TypeNode) => Syntax.Type };
    private readonly valueHandlerMap_: { [index: string]: (node: ast.ValueNode) => Syntax.Literal };

    get definitions() {
        return this.definitions_;
    }

    constructor(scalarMap: ConfigType["scalarMap"], skipTypename: ConfigType["skipTypename"]) {
        this.scalarMap_ = scalarMap;
        this.skipTypename_ = skipTypename;

        this.typeHandlerMap_ = {
            NamedType: (node: ast.NamedTypeNode) => {
                const name = this.visitName(node.name);

                return name in this.scalarMap_ ?
                    new Syntax.Scalar(this.scalarMap_[name])
                    : new Syntax.NamedType(name);
            },
            ListType: (node: ast.ListTypeNode) => {
                let ret = this.visitType(node.type);
                return new Syntax.Array(ret);
            }
        } as { [index: string]: (node: ast.TypeNode) => Syntax.Type };

        this.valueHandlerMap_ = {
            BooleanValue: (node: ast.BooleanValueNode) => new Syntax.Literal(String(node.value)),
            StringValue: (node: ast.StringValueNode) => new Syntax.Literal(`"${node.value}"`),
            FloatValue: (node: ast.FloatValueNode) => new Syntax.Literal(node.value),
            IntValue: (node: ast.IntValueNode) => new Syntax.Literal(node.value),
            NullValue: () => new Syntax.Literal("null"),
            ListValue: (node: ast.ListValueNode) => new Syntax.ArrayLiteral(node.values.map(lit => this.visitValue(lit)))
        } as { [index: string]: (node: ast.ValueNode) => Syntax.Literal };
    }

    ObjectTypeDefinition(node: ast.ObjectTypeDefinitionNode) {
        const name = this.visitName(node.name);
        const props = (node.fields || []).map(field => this.visitFieldDefinition(field, Syntax.AbstractProperty));

        if (!this.skipTypename_) {
            props.push(new Syntax.AbstractProperty(
                "__typename",
                new Syntax.Scalar(this.scalarMap_["String"]),
                new Syntax.Literal(`"${name}"`)
                )
            );
        }

        const interfaces = (node.interfaces || []).map((iface) => this.visitNamedType(iface)) as Syntax.NamedType[];

        this.definitions_.push(new Syntax.AbstractClass(name, undefined, interfaces, props));
    }

    InterfaceTypeDefinition(node: ast.InterfaceTypeDefinitionNode) {
        const name = this.visitName(node.name);
        const props = (node.fields || []).map(field => this.visitFieldDefinition(field, Syntax.PureProperty));

        if (!this.skipTypename_) {
            props.push(new Syntax.PureProperty(
                "__typename",
                new Syntax.Scalar(this.scalarMap_["String"])
                )
            );
        }

        const interfaces = (node.interfaces || []).map((iface) => this.visitNamedType(iface)) as Syntax.NamedType[];

        this.definitions_.push(new Syntax.Interface(name, interfaces, props));
    }

    private visitFieldDefinition<Prop extends typeof Syntax.Property>(node: ast.FieldDefinitionNode, propType: Prop) {
        const name = this.visitName(node.name);
        const args = node.arguments?.map(arg => this.visitInputValueDefinition(arg));
        const tp = this.visitType(node.type);

        const trueRet = args && args.length > 0 ? new Syntax.Function(args, tp) : tp;

        return new propType(name, trueRet);
    }

    private visitInputValueDefinition(node: ast.InputValueDefinitionNode) {
        const name = this.visitName(node.name);
        const tp = this.visitType(node.type);
        const defaultValue = node.defaultValue && this.visitValue(node.defaultValue);

        return new Syntax.Argument(name, tp, defaultValue);
    }

    private visitName(node: ast.NameNode) {
        return node.value
    }

    private visitNamedType(node: ast.NamedTypeNode) {
        return this.typeHandlerMap_[node.kind](node);
    }

    private visitType(node: ast.TypeNode) {
        if (node.kind === "NonNullType") {
            return new Syntax.NonNullable(this.typeHandlerMap_[node.type.kind](node.type));
        } else {
            return new Syntax.Maybe(this.typeHandlerMap_[node.kind](node));
        }
    }

    private visitValue(node: ast.ValueNode) {
        return this.valueHandlerMap_[node.kind](node);
    }

}

export class ConfigType {
    typesPrefix: string;
    typesSuffix: string;
    skipTypename: boolean;
    noExport: boolean;
    scalarMap: Record<"String" | "Boolean" | "ID" | "Float" | "Int" | string, string>;
    generateArgumentDefaults: boolean;
    generatePropertyDefaults: boolean;
    namespaceName: string | null;
}

function marshallConfig(config: ConfigType) {
    const marshalled = {...config};
    marshalled.typesPrefix = config.typesPrefix || "";
    marshalled.typesSuffix = config.typesSuffix || "";
    marshalled.skipTypename = config.skipTypename || false;
    marshalled.noExport = config.noExport || false;
    marshalled.generateArgumentDefaults = config.generateArgumentDefaults || false;
    marshalled.generatePropertyDefaults = config.generatePropertyDefaults || false;
    marshalled.scalarMap = config.scalarMap || {
        "String": "string",
        "Boolean": "boolean",
        "ID": "string",
        "Float": "number",
        "Int": "number",
    }

    return marshalled;
}

module.exports = {
    plugin: (schema: GraphQLSchema, _document: Source[], config: ConfigType) => {
        config = marshallConfig(config);

        const visitor = new TypeVisitor(config.scalarMap, config.skipTypename);
        Object.values(schema.getTypeMap()).forEach((node) => visit(node.astNode!, visitor));

        const mod = new Syntax.Module(visitor.definitions);

        return mod.toString(config);
    }
}
